#Author: Michał Misztal (ciemna.strona.klamki@gmail.com)

class Tab():
    def __init__(self):
        self.table = ''
        self.x = 0
        self.naglowek = ''
        self.dane = ''

    def tab(self):
        self.table = '<table>\n%s\n</table>\n' % (self.table)

    def thead(self,naglowki):
        self.naglowek = '<thead>\n<tr>\n<th>%s</th>\n</tr>\n</thead>\n' % ('</th><th>'.join(naglowki))
        self.x = len(naglowki)
         
    def tbody(self,dane):
        self.dane += '<tbody>'
        i = 0
        for v in dane:
            i += 1
            if self.x != len(v):
                print('Niezgodność danych w linii '+str(i))
            self.dane += '\n<tr><td>%s</td></tr>' % ('</td><td>'.join(v))
        self.dane += '\n</tbody>'

    def pokaz(self):
        self.table = self.naglowek+self.dane
        self.tab()
        return self.table