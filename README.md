python_html_table

Build html table. Usage:

t  = Tab()
t.thead(['a','b','c'])
t.tbody([
    ['1a','1b','1c'],
    ['2a','2b','2c']
])
print(t.pokaz())